# network_currents #

This project allows users to analyze integrated currents in network calculations.  The codes in this project take
as input Webnucleo XML files that include the integrated current data as zone properties.  The *graph_currents*
code computes *dot* files that can be converted to chart of the nuclide diagrams that show the integrated
reaction currents in the network calculations.  The [python](python) codes allow the user to output key data from
the files or compute the integrated current differences between two calculations.

## The graph_currents code ##

### Steps to install ###

First, clone the repository by typing 

**git clone https://bitbucket.org/mbradle/network_currents.git**

Next, ensure that wn_user is installed.  If you have not alreadly done so, type

**git clone https://bitbucket.org/mbradle/wn_user.git**

Change into the *network_currents* directory and create the project by typing

**cd network_currents**

**./project_make**

### Steps to create the diagrams ###

To get started,
retrieve an example xml from [OSF](https://osf.io/zhrfn/) file by typing

**curl -o example.xml -L -J https://osf.io/ytud2/download**

Create a directory, preferably called *output*, where the output [graphviz](https://graphviz.org) files will be stored:

**mkdir output**

Run the *graph_currents* code on a previously created *single_zone_network* output xml file.  As an initial example, use the example input file that you retrieved over the web.  Thus, type

**./graph_currents --libnucnet_xml example.xml --graph_output_base output/out --induced_nuc_xpath "[z >= 6 and z <= 54 and a - z >= 6 and a - z <= 60]"**

This example execution will read the data *example.xml* file and
create the graph integrated current diagrams from those data.  It will create a subset of the full network
with atomic number *Z* in the range 6 <= Z <= 54 and neutron number *N = A - Z*, where *A* is the mass number,
also in the range 6 <= N <= 60.
The output from the execution will be a number
[graphviz](https://graphviz.org) files (labeled *out_x.dot*, where *x* is the
sequential label) in the directory *output*.  To run on your own *single_zone_network* output xml file called, say,
*my_out.xml*, first clean up the output directory by typing

**rm output/\***

Then execute the code on your file by typing, for example:

**./graph_currents --libnucnet_xml my_out.xml --graph_output_base output/out --induced_nuc_xpath "[z >= 6 and z <= 60 and a - z >= 6 and a - z <= 90]"**

Use other options, as desired.  To see the other options available in running the *graph_currents* code, type

**./graph_currents --help**

and

**./graph_currents --prog all**

For example, since you might only be interested in the integrated currents through the entire calculation,
you can select only the last time step.  You might also like to call out weak reactions with red color:

**./graph_currents --libnucnet_xml my_out.xml --graph_output_base output/out --induced_nuc_xpath "[z >= 6 and z <= 60 and a - z >= 6 and a - z <= 90]" --zone_xpath "[last()]" --graph_reaction_color "{[product[contains(.,'neutrino')]]; red}"**

## Steps to create pdf files ##

Once you have created the appropriate *dot* files, you can convert them into pdfs.  To do so, follow these [steps](https://osf.io/36apc/wiki/home/).

## Steps to animate the diagrams ##

It is possible to combine the pdf files previously created into an animation.
To do so, follow these [steps](https://osf.io/792fy/wiki/home/)


