# Copyright 2021 Clemson University
# 
# Author: Bradley S. Meyer
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# To obtain a copy of the GNU General Public License,
# see <https://www.gnu.org/licenses/>.
                         
import argparse
import wnutils.xml as wx 

def partial_match(key, d):
    d_new = {}
    for k, v in d.items():
        if all(k1 == k2 or k2 is None  for k1, k2 in zip(k, key)):
            d_new[k[1]] = float(v)
    return d_new

parser = argparse.ArgumentParser(
            description='Printout species currents.')

parser.add_argument('in_xml', metavar='in_xml', help='input xml')

parser.add_argument('species', metavar='species',
            help='species to find currents for')

parser.add_argument('--zone_xpath', metavar='zone_xpath',
                    default="[last()]",
                    help='zone XPath for xml file (default: "[last()]")')

parser.add_argument('--compare_abunds', metavar='zone_xpath',
                    default=False,
                    help='compare initial and final abunds (default: False)')

args = parser.parse_args()

xml = wx.Xml(args.in_xml)

# Get the reaction data

xpath_reac_in = "[product = '" + args.species + "']"
xpath_reac_out = "[reactant = '" + args.species + "']"

reacs_in = xml.get_reaction_data(reac_xpath = xpath_reac_in)
reacs_out = xml.get_reaction_data(reac_xpath = xpath_reac_out)

# Get zone data

zone_data = xml.get_zone_data(args.zone_xpath)

assert(len(zone_data) == 1)

zone = zone_data[list(zone_data.keys())[0]]

# Get the properties subset with currents

props = zone['properties']

currents = partial_match(('flow current', None), props)

# Get currents

currents_in = []
currents_out = []

for r in reacs_in:
    if r in currents:
        currents_in.append((r, currents[r]))

for r in reacs_out:
    if r in currents:
         currents_out.append((r, currents[r]))

# Sort

currents_in.sort(key = lambda x: x[1], reverse=True)
currents_out.sort(key = lambda x: x[1], reverse=True)

total_in = 0
for tup in currents_in:
    total_in += tup[1]

total_out = 0
for tup in currents_out:
    total_out += tup[1]

# Print out

print()
print('For', args.species, ':')

print()
print('{:55s}    {:11s}'.format('Creation reaction', 'Current'))
print("=" * 55, " " * 2, "=" * 11)
for tup in currents_in:
    print('{:55s}    {:.4e}'.format(tup[0], tup[1]))

print()
print('Total in current: {:.4e}'.format(total_in))
print()

print()
print('{:55s}    {:11s}'.format('Destruction reaction', 'Current'))
print("=" * 55, " " * 2, "=" * 11)
for tup in currents_out:
    print('{:55s}    {:.4e}'.format(tup[0], tup[1]))

print()
print('Total out current: {:.4e}'.format(total_out))
print()

print('Total in current - total out current: {:.4e}'.format(
       total_in - total_out))
print()

# Print out abundances

if args.compare_abunds:

    y_initial = 0
    key = ('initial abundance', args.species)
    if key in props:
        y_initial =  float(props[key])

    y_final = 0
    key = ('final abundance', args.species)
    if key in props:
        y_final =  float(props[key])

    print('Initial abundance: {:.4e}'.format(y_initial))
    print('Final abundance: {:.4e}'.format(y_final))
    print('Final abundance - initial abundance: {:.4e}'.format(
       y_final - y_initial))

    print()
