# python #

This directory contains python routines for analysis of integrated reaction
currents.

- *print_species_currents.py*

This routine prints out the creation and destruction currents for the chosen species.  The default is for the
last zone (step) in the input XML file.  For example, for the default example XML file, type:

**python print_species_currents.py ../example.xml ni56**

to see the currents creating and destroying *<sup>56</sub>Ni* during the network calculation.  To choose a
different zone (step) in the calculation, include a value for the *zone_xpath* option:

**python print_species_currents.py ../example.xml ni56 --zone_xpath "[position() = 20]"**

To compare the *initial* and *final* abundances of the species, use the *compare_abunds* option.  For example,
type:

**python print_species_currents.py ../example.xml ni56 --compare_abunds True**

Ideally, the difference between the initial and final abundances should be equal to the difference between the
total in (creation) current and the total out (desruction) current.  Numerical inaccuracies can sometimes prevent
this from being true.  Also note that the *final* abundance is the abundance at the given step.  The initial
abundance is the abundance at the point the currents start being tracked.

- *compute_current_diff.py*

This routine computes the integrated current difference between two files.  As an example execution, first obtain
a second example XML file by typing:

**curl -o ../example2.xml -L -J https://osf.io/paq63/download**

Then compute the difference between the currents in *../example.xml* and *../example2.xml* by typing:

**python compute_current_diff.py ../example.xml ../example2.xml out.xml**

*out.xml* is a new file with a single zone giving the current differences.  Note that the initial species
abundances in *out.xml* are the final abundances in *../example2.xml* while the final abundances are those
of *example.xml*.  The total in - out currents for a species in *out.xml* should now equal the final - initial 
abundances in *out.xml*; that is, the net current difference into a species should equal the abundance difference
between the two calculations.  You may use *out.xml* in other python codes or the *graph_currents* code.

It is worth noting that it is possible to use the same file as the two input files.  In this case, the resultant
current difference output file holds the difference between two zones in the same calculation.  For example, to
compute the integrated current difference between the 35th and last steps in the calculation recorded in
*../example.xml*, type:

**python compute_current_diff.py ../example.xml ../example.xml out2.xml --zone_xpath_1 "[last()]" --zone_xpath_2 "[position() = 35]"**

The output file *out2.xml* contains the integrated currents between the 35th step and the end of the calculation.
