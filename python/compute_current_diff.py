# Copyright 2021 Clemson University
# 
# Author: Bradley S. Meyer
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# To obtain a copy of the GNU General Public License,
# see <https://www.gnu.org/licenses/>.
                         
import argparse
import wnutils.xml as wx 

def partial_match(key, d):
    d_new = {}
    for k, v in d.items():
        if all(k1 == k2 or k2 is None  for k1, k2 in zip(k, key)):
            d_new[k[1]] = float(v)
    return d_new

def reactions_in_network_check(d, reacs):
    for k in d:
        if k[1] not in reacs:
            print(k[1], ' not in complementary network')
            exit()
 

parser = argparse.ArgumentParser(
            description='Compute current differences and write to XML.')

parser.add_argument('in_xml_1', metavar='in_xml_1', help='first input xml')
parser.add_argument('in_xml_2', metavar='in_xml_2', help='second input xml')
parser.add_argument('out_xml', metavar='out_xml', help='output xml')

parser.add_argument('--zone_xpath_1', metavar='zone_xpath_1',
                    default="[last()]",
                    help='zone XPath for first xml file (default: "[last()]")')
parser.add_argument('--zone_xpath_2', metavar='zone_xpath_2',
                    default="[last()]",
                    help='zone XPath for second xml file (default: "[last()]")')

args = parser.parse_args()

xml1 = wx.Xml(args.in_xml_1)
xml2 = wx.Xml(args.in_xml_2)

# Get the reaction data

reacs1 = xml1.get_reaction_data()
reacs2 = xml2.get_reaction_data()

# Check the reactions

for r in reacs1:
    if r not in reacs2:
        print(r, ' not in both networks')
        exit()

# Get zone data from each file and check that each is a single zone

zone_data_1 = xml1.get_zone_data(args.zone_xpath_1)
zone_data_2 = xml2.get_zone_data(args.zone_xpath_2)

assert(len(zone_data_1) == 1)
assert(len(zone_data_2) == 1)


zone1 = zone_data_1[list(zone_data_1.keys())[0]]
zone2 = zone_data_2[list(zone_data_2.keys())[0]]

# Get the properties subset with currents

currents1 = partial_match(('flow current', None), zone1['properties'])
currents2 = partial_match(('flow current', None), zone2['properties'])

# Create new zone and oop on reactions to add current differences

new_zone = zone1.copy()

for r in reacs1:
    curr = 0
    if r in currents1:
        curr += currents1[r]
    if r in currents2:
        curr -= currents2[r]
    if curr != 0:
        new_zone['properties'][('flow current', r)] = curr
    else:
        if ('flow current', r) in new_zone['properties']:
            new_zone['properties'].pop(('flow current', r))

# Reset initial abundances as final abundances in xml2

y_initial = partial_match(('initial abundance', None), new_zone['properties'])
for key in y_initial:
    new_zone['properties'].pop(('initial abundance', key))

y_initial = partial_match(('final abundance', None), zone2['properties'])
for key in y_initial:
    new_zone['properties'][('initial abundance', key)] = y_initial[key]

# Create and write out new file

new_zone_data = {}
new_zone_data['0'] = new_zone

new_xml = wx.New_Xml(xml_type='libnucnet_input')

new_xml.set_nuclide_data(xml1.get_nuclide_data())
new_xml.set_reaction_data(xml1.get_reaction_data())
new_xml.set_zone_data(new_zone_data)

new_xml.write(args.out_xml)


